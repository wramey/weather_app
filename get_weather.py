import json
import logging
import requests
import traceback

log = logging.getLogger()
log.setLevel(logging.INFO)

#notes
DARK_SKY_SECRET_KEY="d21b17405d692b8977dd9098c59754eb"

def lambda_handler(event, context):
    try:
        query_params = event['queryStringParameters']

        if not ('latitude' in query_params and 'longitude' in query_params):
            return {
                "statusCode": 400,
                "body": "Must include both 'latitude' and 'longitude' query parameters"
            }

        latitude = query_params['latitude']
        longitude = query_params['longitude']
        api_url = f"https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}"

        data = requests.get(api_url)
        log.info("Recieved HTTP %s from Dark Sky" % data.status_code)
        if data.status_code == 400:
            return {
                "statusCode": 400,
                "body": "Invalid Request to dark sky: %s" % data.text
            }
        elif data.status_code == 500:
            return {
                "statusCode": 500,
                "body": "Error from dark sky: %s" % data.text
            }
        response_dict = data.json()
        current_forecast = response_dict.get("currently") 
        daily_forecast = response_dict.get("daily") 
        today_daily_forecast = response_dict.get("daily").get("data")[0] 
        forecast = {
            "summary": current_forecast.get("summary"),
            "temperature": current_forecast.get("temperature"),
            "humidity": current_forecast.get("humidity"),
            "apparentTemperature": current_forecast.get("apparentTemperature"),
            "high": today_daily_forecast.get("temperatureHigh"),
            "low": today_daily_forecast.get("temperatureLow")
        }
        return {
            "body": json.dumps(forecast),
            "statusCode": 200
        }
    except:
        log.error(traceback.format_exc())
        return {
            "statusCode": 500,
            "body": "Error executing lambda"
        }