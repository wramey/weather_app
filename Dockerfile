
FROM python:alpine3.7

LABEL version="1.0" maintainer="Billy Ramey <billy.ramey@libertymutual.com>"

RUN pip3 install requests

COPY . /app

WORKDIR /app

CMD python ./weather.py
