# !/usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY = "d21b17405d692b8977dd9098c59754eb"


def get_location():
    '''Get location from url
    '''
    #response = requests.get("https://ipvigilante.com").json()
    response = requests.get("https://5eaw67w0e2.execute-api.us-east-2.amazonaws.com/prod/location").json()
    long = response['Longitude']
    lat = response['Latitude']
    city = response['City Name']
    return long, lat, city


def get_temperature(Latitude, Longitude, City):
    '''Get temp from location
    '''
    #baseurl = "https://api.darksky.net/forecast"
    url = f"https://5eaw67w0e2.execute-api.us-east-2.amazonaws.com/prod/weather?latitude={Latitude}&longitude={Longitude}"
    #url = baseurl + "/" + DARK_SKY_SECRET_KEY + "/" + Latitude + "," + Longitude
    request = requests.get(url).json()
    temp = request['temperature']
    summary = request['summary']
    high = request['high']
    low = request['low']
    hum = request['humidity']
    feels = request['apparentTemperature']
    return print_forecast(temp, summary, high, low, hum, feels)


def print_forecast(temp, summary, high, low, hum, feels):
    """ Prints the weather forecast given the specified temperature
    """
    print("\nThe weather forecast for " + str(City) + "\n")
    print("   It is " + str(summary) + " Outside Today\n")
    print("Today's temperature is: " + str(temp) + " degrees!")
    print("Today's high is: " + str(high) + " degrees")
    print("Today's low is: " + str(low) + " degrees")
    print("Humidity: " + str(hum) + "%")
    print("Feels Like: " + str(feels) + " degrees outside\n")


if __name__ == "__main__":
    Longitude, Latitude, City = get_location()
    get_temperature(Latitude, Longitude, City)
