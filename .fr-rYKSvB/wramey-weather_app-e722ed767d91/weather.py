# !/usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY = "d21b17405d692b8977dd9098c59754eb"


def get_location():
    '''Get location from url
    '''
    response = requests.get("https://ipvigilante.com").json()
    long = response['data']['longitude']
    lat = response['data']['latitude']
    city = response['data']['city_name']
    return long, lat, city


def get_temperature(Latitude, Longitude, City):
    '''Get temp from location
    '''
    baseurl = "https://api.darksky.net/forecast"
    url = baseurl + "/" + DARK_SKY_SECRET_KEY + "/" + Latitude + "," + Longitude  # noqa
    request = requests.get(url).json()
    temp = request['currently']['temperature']
    summary = request['currently']['summary']
    high = request['daily']['data'][0]['temperatureHigh']
    low = request['daily']['data'][0]['temperatureLow']
    hum = request['currently']['humidity']
    feels = request['currently']['apparentTemperature']
    return print_forecast(temp, summary, high, low, hum, feels)


def print_forecast(temp, summary, high, low, hum, feels):
    """ Prints the weather forecast given the specified temperature
    """
    print("\nThe weather forecast for " + str(City) + "\n")
    print("   It is " + str(summary) + " Outside Today\n")
    print("Today's temperature is: " + str(temp) + " degrees!")
    print("Today's high is: " + str(high) + " degrees")
    print("Today's low is: " + str(low) + " degrees")
    print("Humidity: " + str(hum) + "%")
    print("Feels Like: " + str(feels) + " degrees outside\n")


if __name__ == "__main__":
    Longitude, Latitude, City = get_location()
    get_temperature(Latitude, Longitude, City)
